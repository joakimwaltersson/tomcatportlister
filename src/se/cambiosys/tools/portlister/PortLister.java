package se.cambiosys.tools.portlister;

import java.io.File;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class PortLister
{

  /**
   * @param args
   */
  public static void main(String[] args)
  {
    PortLister portLister = new PortLister();
    portLister.printPorts();
  }

  private void printPorts()
  {
    File dir = new File(".");
    Map<String, File> configFiles = getConfigFiles(dir);
    if (configFiles.isEmpty())
    {
      printUsage();
    }
    else
    {
      for (Entry<String, File> file : configFiles.entrySet())
      {
        System.out.println(file.getKey());
        System.out.println("============");
        printPorts(file.getValue());
        System.out.println();
      }
    }
  }

  private void printUsage()
  {
    System.out.println("PortLister lists ports configured from multiple Tomcat installations."
        + " PortLister ONLY tries to find configuration files 2 levels below the current one.");
  }

  private void printPorts(File file)
  {
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    try
    {
      DocumentBuilder builder = factory.newDocumentBuilder();
      Document doc = builder.parse(file);
      XPathFactory xPathfactory = XPathFactory.newInstance();
      XPath xpath = xPathfactory.newXPath();
      XPathExpression expr = xpath.compile("/Server/@port");
      String serverPort = expr.evaluate(doc);
      expr = xpath.compile("/Server/Service/Connector");
      Object connectorPort = expr.evaluate(doc, XPathConstants.NODESET);
      System.out.println("Shutdown=" + serverPort);
      NodeList nodes = (NodeList) connectorPort;
      for (int i = 0; i < nodes.getLength(); i++)
      {
        Node item = nodes.item(i);
        String attr = "protocol";
        System.out.print(getAttributeValue(item, attr) + "=");
        System.out.println(getAttributeValue(item, "port"));
      }
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }

  private String getAttributeValue(Node item, String attr)
  {
    Node namedItem = item.getAttributes().getNamedItem(attr);
    if (namedItem != null)
    {
      return namedItem.getNodeValue();
    }
    return "<unknown>";
  }

  private Map<String, File> getConfigFiles(File dir)
  {
    File[] files = dir.listFiles();
    Map<String, File> result = new TreeMap<String, File>();
    for (File file : files)
    {
      if (file.isDirectory())
      {
        File confDir = new File(file, "conf");
        if (confDir.exists() && confDir.isDirectory())
        {
          File configFile = new File(confDir, "server.xml");
          if (configFile.exists())
          {
            result.put(file.getName(), configFile);
          }
        }
      }
    }
    return result;
  }

}
